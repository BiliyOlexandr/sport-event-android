package com.sportevent.app.util;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


/**
 * можешь это юзануть для смены фрагментов, можешь свой.
 */
public class ActivityUtils  {

    public static void addFragmentToActivity (FragmentManager fragmentManager,
                                              @NonNull Fragment fragment, int frameId) {
        if (fragmentManager == null){return;}
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(frameId, fragment);
        transaction.commitAllowingStateLoss();
    }

    public static void replaceFragmentToActivity (FragmentManager fragmentManager,
                                                  @NonNull Fragment fragment, int frameId, boolean stack) {
        if (fragmentManager == null){return;}
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(frameId, fragment);
        if (stack) {
            transaction.addToBackStack(null);
        }
        transaction.commitAllowingStateLoss();
    }

}
