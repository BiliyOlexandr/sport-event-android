package com.sportevent.app.service

import com.sportevent.app.data.model.*
import io.reactivex.Observable
import retrofit2.http.*

interface APIService {

    @FormUrlEncoded
    @POST("api/common/getCityGeo/")
    fun getCityByGeo(@FieldMap args: HashMap<String, Any?>?): Observable<CityResponse>

    @FormUrlEncoded
    @POST("api/common/getCountry/")
    fun getCountriesList(@Field("lang") lang: String): Observable<CountriesResponse>

    @FormUrlEncoded
    @POST("api/common/registration/")
    fun signUpSimple(@FieldMap args: HashMap<String, Any?>?) : Observable<SignUpResponse>

    @FormUrlEncoded
    @POST("api/common/login/")
    fun signInSimple(@FieldMap args: HashMap<String, Any?>?) : Observable<SignUpResponse>

    @FormUrlEncoded
    @POST("api/common/loginFirebase/")
    fun googleSignIn(@FieldMap args: HashMap<String, Any?>?): Observable<SignUpResponse>

    @FormUrlEncoded
    @POST("api/common/loginVK/")
    fun vkSignIn(@FieldMap args: HashMap<String, Any?>?): Observable<SignUpResponse>

    @FormUrlEncoded
    @POST("api/common/loginFB/")
    fun fbSignIn(@FieldMap args: HashMap<String, Any?>?): Observable<SignUpResponse>

    @FormUrlEncoded
    @POST("api/common/recoveryPass/")
    fun resporePassword(@Field("email") email: String): Observable<SimpleResponse>

    @FormUrlEncoded
    @POST("api/common/getCommonData/")
    fun getDictionaries(@Header("Authorization") token: String, @Field("lang") lang: String): Observable<DictionariesResponse>

    @FormUrlEncoded
    @POST("api/user/getUser")
    fun getUserInfo(@Header("Authorization") token: String): Observable<UserResponse>

    @FormUrlEncoded
    @POST("api/user/setUser")
    fun updateUserInfo(@Header("Authorization") token: String, @FieldMap args: HashMap<String, Any?>?): Observable<SimpleResponse>

    @FormUrlEncoded
    @POST("api/user/getEvents/")
    fun getEvents(@Header("Authorization") token: String): Observable<EventsResponse>

    @FormUrlEncoded
    @POST("api/user/createEvent/")
    fun createEvent(@Header("Authorization") token: String, @FieldMap args: HashMap<String, Any?>?): Observable<EventResponse>

    @FormUrlEncoded
    @POST("api/user/publicEvent/")
    fun publishEvent(@Header("Authorization") token: String, @Field("id") id: Int): Observable<EventResponse>

    @FormUrlEncoded
    @POST("api/user/delEvent/")
    fun deleteEvent(@Header("Authorization") token: String, @Field("id") id: Int): Observable<SimpleResponse>
}
