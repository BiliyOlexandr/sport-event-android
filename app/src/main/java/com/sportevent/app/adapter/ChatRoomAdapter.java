package com.sportevent.app.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.data.model.ChatRoomModel;

import java.util.List;

public class ChatRoomAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static final int LAYOUT_ONE= 0;
    private static final int LAYOUT_TWO= 1;

    private final List<ChatRoomModel> items;

    public ChatRoomAdapter(List<ChatRoomModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    class ViewHolder0 extends RecyclerView.ViewHolder {

        public TextView text;

        public ViewHolder0(View itemView){
            super(itemView);
            text = itemView.findViewById(R.id.text);
        }

        void bind(final ChatRoomModel item) {
            text.setText(item.getText());
        }
    }

    class ViewHolder2 extends RecyclerView.ViewHolder {

        public TextView text;

        public ViewHolder2(View itemView) {
            super(itemView);
            text = itemView.findViewById(R.id.text);
        }

        void bind(final ChatRoomModel item) {
            text.setText(item.getText());
        }
    }

    @Override
    public int getItemViewType(int position) {

        if (items.get(position).isMyItem()){
            return LAYOUT_ONE;
        } else {
            return LAYOUT_TWO;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view;
        RecyclerView.ViewHolder viewHolder;

        if (viewType == LAYOUT_ONE){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_message, parent, false);
            viewHolder = new ViewHolder0(view);
        } else {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_message, parent, false);
            viewHolder = new ViewHolder2(view);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (holder.getItemViewType() == LAYOUT_ONE){

        } else {

        }

    }

    @Override public int getItemCount() {
        return items.size();
    }

    public void addItem(ChatRoomModel item) {
        items.add(item);
        notifyItemInserted(items.size() - 1);
    }
}
