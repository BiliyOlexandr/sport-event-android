package com.sportevent.app.adapter;

import android.app.Dialog;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.view.menu.MenuBuilder;
import androidx.appcompat.view.menu.MenuPopupHelper;
import androidx.appcompat.widget.PopupMenu;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.data.model.Feedback;

import java.util.List;

public class RequestDetailsAdapter extends RecyclerView.Adapter<RequestDetailsAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(Feedback item);
    }

    private static List<Feedback> items;
    private final OnItemClickListener listener;

    public RequestDetailsAdapter(List<Feedback> items, OnItemClickListener listener) {
        this.items = items;
        this.listener = listener;
        notifyDataSetChanged();
    }

    @NonNull
    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.request_details_row, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(items.get(position), listener, position);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView userName, location, eventTitle, eventDescription;
        private ImageView eventImage, avatar, sportType;
        private CardView shareBtn;
        private FrameLayout moreBtn, cancelBtn;

        ViewHolder(View itemView) {
            super(itemView);
            cancelBtn = itemView.findViewById(R.id.cancelBtn);
            moreBtn = itemView.findViewById(R.id.moreBtn);
//            userName = itemView.findViewById(R.id.userName);
//            location = itemView.findViewById(R.id.location);
//            eventTitle = itemView.findViewById(R.id.eventTitle);
//            eventDescription = itemView.findViewById(R.id.eventDescription);
//            eventImage = itemView.findViewById(R.id.eventImage);
//            avatar = itemView.findViewById(R.id.avatar);
//            sportType = itemView.findViewById(R.id.sportType);
//            shareBtn = itemView.findViewById(R.id.shareBtn);
        }

        void bind(final Feedback item, final OnItemClickListener listener, final int position) {
//            userName.setText(item.getName());
//            eventImage.setImageResource(item.getImage());
//            moreBtn.setOnClickListener(new View.OnClickListener() {
//                @Override public void onClick(View v) {
//                    listener.onItemClick(item);
//                }
//            });

            cancelBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final Dialog dialog = new Dialog(itemView.getContext(), R.style.CustomDialog);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.dialog_delete_user);

                    CardView text = (CardView) dialog.findViewById(R.id.deleteBtn);
                    text.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialog.dismiss();
                            Toast.makeText(itemView.getContext(), "Юзер удален", Toast.LENGTH_SHORT).show();
                            items.remove(position);
                            notifyItemRemoved(position);
                            notifyItemRangeChanged(position, items.size());
                        }
                    });

                    CardView dialogButton = (CardView) dialog.findViewById(R.id.cancelBtn);
                    dialogButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    dialog.show();
                }
            });

            moreBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PopupMenu popup = new PopupMenu(itemView.getContext(), moreBtn);
                    popup.inflate(R.menu.popup_menu);
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            return true;
                        }
                    });
                    MenuPopupHelper menuHelper = new MenuPopupHelper(itemView.getContext(), (MenuBuilder) popup.getMenu(), moreBtn);
                    menuHelper.setForceShowIcon(true);
                    menuHelper.show();
                }
            });
        }
    }
}
