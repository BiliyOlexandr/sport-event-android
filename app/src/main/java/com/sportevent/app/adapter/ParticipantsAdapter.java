package com.sportevent.app.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.data.model.RequestModel;

import java.util.List;

public class ParticipantsAdapter extends RecyclerView.Adapter<ParticipantsAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(RequestModel item);
    }

    private final List<RequestModel> items;
    private final OnItemClickListener listener;

    public ParticipantsAdapter(List<RequestModel> items, OnItemClickListener listener) {
        this.items = items;
        this.listener = listener;
        notifyDataSetChanged();
    }

    @NonNull
    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.participant_row, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(items.get(position), listener);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView tvShortName, tvName, tvCity;
        private LinearLayout rootView;

        ViewHolder(View itemView) {
            super(itemView);

            tvShortName = itemView.findViewById(R.id.tvShortName);
            tvName = itemView.findViewById(R.id.tvName);
            tvCity = itemView.findViewById(R.id.tvCity);
            rootView = itemView.findViewById(R.id.rootView);
        }

        void bind(final RequestModel item, final OnItemClickListener listener) {
            tvShortName.setText(item.getShortName());
            tvName.setText(item.getName());
            tvCity.setText(item.getCity());
//            eventImage.setImageResource(item.getImage());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }
}
