package com.sportevent.app.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.data.model.FollowerModel;

import java.util.List;

public class FollowersAdapter extends RecyclerView.Adapter<FollowersAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(FollowerModel item);
    }

    private final List<FollowerModel> items;
    private final OnItemClickListener listener;

    public FollowersAdapter(List<FollowerModel> items, OnItemClickListener listener) {
        this.items = items;
        this.listener = listener;
        notifyDataSetChanged();
    }

    @NonNull
    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.follower_row, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(items.get(position), listener);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private CardView follow, unfollow;
        private LinearLayout rootView;

        ViewHolder(View itemView) {
            super(itemView);

            follow = itemView.findViewById(R.id.follow);
            unfollow = itemView.findViewById(R.id.unfollow);
            rootView = itemView.findViewById(R.id.rootView);

//            location = itemView.findViewById(R.id.location);
//            eventTitle = itemView.findViewById(R.id.eventTitle);
//            eventDescription = itemView.findViewById(R.id.eventDescription);
//            eventImage = itemView.findViewById(R.id.eventImage);
//            avatar = itemView.findViewById(R.id.avatar);
//            sportType = itemView.findViewById(R.id.sportType);
//            shareBtn = itemView.findViewById(R.id.shareBtn);
        }

        void bind(final FollowerModel item, final OnItemClickListener listener) {
//            userName.setText(item.getName());
//            eventImage.setImageResource(item.getImage());
            if (item.isFollowing()){
                follow.setVisibility(View.GONE);
                unfollow.setVisibility(View.VISIBLE);
            } else {
                unfollow.setVisibility(View.GONE);
                follow.setVisibility(View.VISIBLE);
            }

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }
}
