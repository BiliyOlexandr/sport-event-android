package com.sportevent.app.adapter;

import android.graphics.Color;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.data.model.EventModel;

import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(EventModel item);
    }

    public interface OnItemClickListener2 {
        void onItemClick();
    }

    private final List<EventModel> items;
    private final OnItemClickListener listener;
    private final OnItemClickListener2 listener2;

    public EventsAdapter(List<EventModel> items, OnItemClickListener listener, OnItemClickListener2 listener2) {
        this.items = items;
        this.listener = listener;
        this.listener2 = listener2;
        notifyDataSetChanged();
    }

    @NonNull
    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_row, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(items.get(position), listener, listener2);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView userName, location, eventTitle, eventDescription;
        private ImageView eventImage, avatar, sportType;
        private CardView shareBtn;
        private LinearLayout userLayout;

        ViewHolder(View itemView) {
            super(itemView);
            userName = itemView.findViewById(R.id.userName);
            location = itemView.findViewById(R.id.location);
            eventTitle = itemView.findViewById(R.id.eventTitle);
            eventDescription = itemView.findViewById(R.id.eventDescription);
            eventImage = itemView.findViewById(R.id.eventImage);
            avatar = itemView.findViewById(R.id.avatar);
            sportType = itemView.findViewById(R.id.sportType);
            shareBtn = itemView.findViewById(R.id.shareBtn);
            userLayout = itemView.findViewById(R.id.userLayout);
        }

        void bind(final EventModel item, final OnItemClickListener listener, final OnItemClickListener2 listener2) {
//            userName.setText(item.getName());
//            eventImage.setImageResource(item.getImage());
            eventDescription.setText(
                    Html.fromHtml(cutDescription(itemView.getContext().getString(R.string.test_text),
                    itemView.getContext().getString(R.string.more)))
            );
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
            avatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener2.onItemClick();
                }
            });

            userLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener2.onItemClick();
                }
            });

            shareBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }

    private static String cutDescription(String s, String m){
        String newString = s.substring(0, Math.min(s.length(), 70));
        SpannableString ss = new SpannableString(m);
        ss.setSpan(new ForegroundColorSpan(Color.GREEN), 0, 2, 0);
        return newString + " " + m;
    }
}
