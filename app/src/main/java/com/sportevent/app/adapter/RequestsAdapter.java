package com.sportevent.app.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.data.model.Feedback;

import java.util.List;

public class RequestsAdapter extends RecyclerView.Adapter<RequestsAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(Feedback item);
    }

    private final List<Feedback> items;
    private final OnItemClickListener listener;

    public RequestsAdapter(List<Feedback> items, OnItemClickListener listener) {
        this.items = items;
        this.listener = listener;
        notifyDataSetChanged();
    }

    @NonNull
    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.request_row, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(items.get(position), listener);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView userName, location, eventTitle, eventDescription;
        private ImageView eventImage, avatar, sportType;
        private CardView shareBtn;

        ViewHolder(View itemView) {
            super(itemView);
//            userName = itemView.findViewById(R.id.userName);
//            location = itemView.findViewById(R.id.location);
//            eventTitle = itemView.findViewById(R.id.eventTitle);
//            eventDescription = itemView.findViewById(R.id.eventDescription);
//            eventImage = itemView.findViewById(R.id.eventImage);
//            avatar = itemView.findViewById(R.id.avatar);
//            sportType = itemView.findViewById(R.id.sportType);
//            shareBtn = itemView.findViewById(R.id.shareBtn);
        }

        void bind(final Feedback item, final OnItemClickListener listener) {
//            userName.setText(item.getName());
//            eventImage.setImageResource(item.getImage());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }
}
