package com.sportevent.app.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.data.model.EventOrganizerModel;

import java.util.List;

public class OrganizerAdapter extends RecyclerView.Adapter<OrganizerAdapter.ViewHolder> {

    public interface OnItemClickListener {
        void onItemClick(EventOrganizerModel item);
    }

    private final List<EventOrganizerModel> items;
    private final OnItemClickListener listener;

    public OrganizerAdapter(List<EventOrganizerModel> items, OnItemClickListener listener) {
        this.items = items;
        this.listener = listener;
        notifyDataSetChanged();
    }

    @NonNull
    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.organizer_row, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(items.get(position), listener);
    }

    @Override public int getItemCount() {
        return items.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private TextView count;
        private ImageView userImage;

        ViewHolder(View itemView) {
            super(itemView);
            count = itemView.findViewById(R.id.count);
            userImage = itemView.findViewById(R.id.userImage);
        }

        void bind(final EventOrganizerModel item, final OnItemClickListener listener) {
//            userName.setText(item.getName());
//            eventImage.setImageResource(item.getImage());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }
}
