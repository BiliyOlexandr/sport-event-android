package com.sportevent.app.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.pixplicity.easyprefs.library.Prefs;
import com.sportevent.app.R;
import com.sportevent.app.data.model.FilterModel;

import java.util.List;

public class SportFilterAdapter extends RecyclerView.Adapter<SportFilterAdapter.ViewHolder> {

    private final List<FilterModel> items;
    private static int lastCheckedPosition = Prefs.getInt("selected_position", 0);

    public SportFilterAdapter(List<FilterModel> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.sport_filter_row, parent, false);
        return new ViewHolder(v);
    }

    @Override public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(items.get(position));
    }

    @Override public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private CheckBox selector;

        ViewHolder(View itemView) {
            super(itemView);
            selector = itemView.findViewById(R.id.selector);
        }

        void bind(final FilterModel item) {

            selector.setChecked(getAdapterPosition() == lastCheckedPosition);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    lastCheckedPosition = getAdapterPosition();
                    notifyDataSetChanged();
//                    selector.setChecked(true);
                    Prefs.putInt("selected_position", item.getId());
                }
            });
        }
    }
}
