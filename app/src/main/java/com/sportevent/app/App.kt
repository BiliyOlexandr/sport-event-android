package com.sportevent.app

import android.app.Application
import android.content.ContextWrapper
import androidx.room.Room
import com.github.salomonbrys.kodein.*

import com.pixplicity.easyprefs.library.Prefs
import com.sportevent.app.data.Repository
import com.sportevent.app.data.local.AppDatabase
import com.sportevent.app.data.remote.RetrofitFactory
import com.sportevent.app.service.APIService
import com.sportevent.app.util.AppExecutors
import com.vk.sdk.VKSdk

class App : Application(), KodeinAware {

    override val kodein by Kodein.lazy {
        bind<APIService>() with provider { RetrofitFactory.getFactory(applicationContext.getString(R.string.api_base_url)).create(APIService::class.java) }

        bind<AppDatabase>() with eagerSingleton {
            Room.databaseBuilder(this@App, AppDatabase::class.java, "sportevents_database")
                    .allowMainThreadQueries()
                    .fallbackToDestructiveMigration()
                    .build()
        }
    }

    //класс с потоками
    var appExecutors: AppExecutors? = null
        private set

    /**
     * Сюда пихай глобальные классы. Типа юзера данные.
     */


    override fun onCreate() {
        super.onCreate()
        instance = this
        appExecutors = AppExecutors()
        Repository.createRepository(
                appExecutors
        )

        Prefs.Builder()
                .setContext(this)
                .setMode(ContextWrapper.MODE_PRIVATE)
                .setPrefsName(packageName)
                .setUseDefaultSharedPreference(true)
                .build()

        VKSdk.initialize(applicationContext)
    }

    companion object {

        var instance: App? = null
            private set
    }


}
