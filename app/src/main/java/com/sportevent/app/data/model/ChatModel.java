package com.sportevent.app.data.model;

public class ChatModel {

    private Boolean isNew;
    private Integer count;

    public ChatModel(Boolean isNew, Integer count) {
        this.isNew = isNew;
        this.count = count;
    }

    public Boolean getNew() {
        return isNew;
    }

    public void setNew(Boolean aNew) {
        isNew = aNew;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
