package com.sportevent.app.data.local

import androidx.room.*
import com.sportevent.app.data.model.CountryModel
import com.sportevent.app.data.model.SportKindModel
import com.sportevent.app.data.model.TypeEventModel

@Dao
interface TypeEventDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(typeEvent: TypeEventModel)

    @Delete
    fun delete(typeEvent: TypeEventModel)

    @Update
    fun update(typeEvent: TypeEventModel)

    @Query("SELECT * FROM type_event WHERE id = :id")
    fun getById(id: Int): TypeEventModel

    @Query("SELECT * FROM sport_kind")
    fun getAll(): List<TypeEventModel>
}