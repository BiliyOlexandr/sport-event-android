package com.sportevent.app.data.model;

public class FollowerModel {
    private boolean isFollowing;

    public FollowerModel(boolean isFollowing) {
        this.isFollowing = isFollowing;
    }

    public boolean isFollowing() {
        return isFollowing;
    }

    public void setFollowing(boolean following) {
        isFollowing = following;
    }
}
