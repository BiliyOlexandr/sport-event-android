package com.sportevent.app.data.local

import androidx.room.*
import com.sportevent.app.data.model.CountryModel
import com.sportevent.app.data.model.SportKindModel
import com.sportevent.app.data.model.TypeEventModel
import com.sportevent.app.data.model.TypeParticipantModel

@Dao
interface TypeParticipantDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(typeParticipant: TypeParticipantModel)

    @Delete
    fun delete(typeParticipant: TypeParticipantModel)

    @Update
    fun update(typeParticipant: TypeParticipantModel)

    @Query("SELECT * FROM type_participant WHERE id = :id")
    fun getById(id: Int): TypeParticipantModel

    @Query("SELECT * FROM type_participant")
    fun getAll(): List<TypeParticipantModel>
}