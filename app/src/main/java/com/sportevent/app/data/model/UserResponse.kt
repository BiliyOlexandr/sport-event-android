package com.sportevent.app.data.model

import com.google.gson.annotations.SerializedName

class UserResponse {

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("comment")
    var comment: String? = null

    @SerializedName("user")
    var user: UserModel? = null
//    @SerializedName("filter")
    @SerializedName("new_message")
    var newMessage: Int? = null
//    @SerializedName("events")
//    @SerializedName("follower_users")
//    @SerializedName("participants")
//    @SerializedName("user_participant")
//    var userParticipant: List<>
}