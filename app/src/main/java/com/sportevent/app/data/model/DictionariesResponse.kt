package com.sportevent.app.data.model

import com.google.gson.annotations.SerializedName

class DictionariesResponse {

    @SerializedName("sport_kind")
    var sportKkind: List<SportKindModel>? = null

    @SerializedName("type_participant")
    var typeParticipant: List<TypeParticipantModel>? = null

    @SerializedName("type_event")
    var typeEvent: List<TypeEventModel>? = null

    @SerializedName("country")
    var country: List<CountryModel>? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("comment")
    var comment: String? = null

}