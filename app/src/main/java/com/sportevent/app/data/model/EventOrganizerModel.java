package com.sportevent.app.data.model;

import java.io.Serializable;

public class EventOrganizerModel implements Serializable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
