package com.sportevent.app.data;


import android.util.Log;
import com.sportevent.app.util.AppExecutors;


/**
 * Запихивай сюда все методы которые делают загрузку данных с сервера / БД / Префернс
 * remote - папка для класов сетевых запросов
 * local - для румов/БД/префернса
 * Доступ к этому классу в классе App
 */

public class Repository {
    private static Repository instance = null;
    private Repository(AppExecutors appExecutors){
        this.appExecutors = appExecutors;
    }

    public static Repository getInstance(){
        return instance;
    }

    public static Repository createRepository(AppExecutors appExecutors) {
        if (instance == null) {
            synchronized (Repository.class) {
                if (instance == null) instance = new Repository(appExecutors);
            }
        }
        return instance;
    }


    String TAG = "Repository";
    private AppExecutors appExecutors;




    public void registartion () {
        appExecutors.networkIO().execute(new Runnable() {
            @Override
            public void run() {
               //поток сетевой
               appExecutors.mainThread().execute(new Runnable() {
                   @Override
                   public void run() {
                       //главный поток
                   }
               });
            }
        });
    }




}
