package com.sportevent.app.data.model

import com.google.gson.annotations.SerializedName

class SignUpResponse {

    @SerializedName("token")
    var token: String? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("comment")
    var comment: String? = null
}