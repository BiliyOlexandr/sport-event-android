package com.sportevent.app.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "sport_kind")
open class SportKindModel {

    @PrimaryKey
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("icon")
    var icon: String? = null

    @SerializedName("name")
    var name: String? = null
}