package com.sportevent.app.data.model;

public class ReportModel {
    public String title;

    public ReportModel(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
