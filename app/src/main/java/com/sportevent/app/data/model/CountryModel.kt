package com.sportevent.app.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "countries")
open class CountryModel {

    @PrimaryKey
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("iso")
    var iso: String? = null

    @SerializedName("name")
    var name: String? = null
}