package com.sportevent.app.data.model

import com.google.gson.annotations.SerializedName

class CountriesResponse {

    @SerializedName("country")
    var countries: List<CountryModel>? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("comment")
    var comment: String? = null
}