package com.sportevent.app.data.model

import com.google.gson.annotations.SerializedName

class EventResponse {

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("comment")
    var comment: String? = null

    @SerializedName("event")
    var event: EventsModel? = null
}