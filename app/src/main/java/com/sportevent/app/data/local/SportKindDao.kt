package com.sportevent.app.data.local

import androidx.room.*
import com.sportevent.app.data.model.CountryModel
import com.sportevent.app.data.model.SportKindModel

@Dao
interface SportKindDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(sportKind: SportKindModel)

    @Delete
    fun delete(sportKind: SportKindModel)

    @Update
    fun update(sportKind: SportKindModel)

    @Query("SELECT * FROM sport_kind WHERE id = :id")
    fun getById(id: Int): SportKindModel

    @Query("SELECT * FROM sport_kind")
    fun getAll(): List<SportKindModel>
}