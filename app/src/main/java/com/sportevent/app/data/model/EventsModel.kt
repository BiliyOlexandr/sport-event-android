package com.sportevent.app.data.model

import com.google.gson.annotations.SerializedName

class EventsModel{

    @SerializedName("sport_kind_id")
    var sportKindId: Int? = null

    @SerializedName("type_event_id")
    var typEventId: Int? = null

    @SerializedName("gender")
    var gender: Int? = null

    @SerializedName("published")
    var published: Int? = null

    @SerializedName("date_start")
    var dateStart: Long? = null

    @SerializedName("type_participant_id")
    var typeParticipantId: Int? = null

    @SerializedName("date_stop")
    var dateStop: Long? = null

    @SerializedName("participant_sum")
    var participantSum: Int? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("dir_photo")
    var dirPhoto: String? = null

    @SerializedName("participant_ante")
    var participantAnte: String? = null

    @SerializedName("description_ante")
    var descriptionAnte: String? = null

    @SerializedName("prize_pool")
    var prizePool: String? = null

    @SerializedName("country_id")
    var countryId: Int? = null

    @SerializedName("city_id")
    var cityId: Int? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("created")
    var created: Long? = null

    @SerializedName("user_id")
    var userId: Int? = null

    @SerializedName("participant_count")
    var participantCount: Int? = null

    @SerializedName("age_min")
    var ageMin: Int? = null

    @SerializedName("age_max")
    var ageMax: Int? = null

    @SerializedName("id")
    var id: Int? = null
}
