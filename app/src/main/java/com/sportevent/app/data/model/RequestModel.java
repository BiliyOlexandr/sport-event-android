package com.sportevent.app.data.model;

public class RequestModel {
    private String name;
    private String city;
    private String shortName;

    public RequestModel(String name, String city, String shortName) {
        this.name = name;
        this.city = city;
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
}
