package com.sportevent.app.data.model

import com.google.gson.annotations.SerializedName

class CityModel {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("country")
    var country: String? = null

    @SerializedName("lat")
    var lat: Double? = null

    @SerializedName("lng")
    var lng: Double? = null

    @SerializedName("name")
    var name: String? = null
}