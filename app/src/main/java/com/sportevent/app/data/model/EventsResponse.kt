package com.sportevent.app.data.model

import com.google.gson.annotations.SerializedName

class EventsResponse {

    @SerializedName("events")
    var events: List<EventsModel>? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("comment")
    var comment: String? = null
}