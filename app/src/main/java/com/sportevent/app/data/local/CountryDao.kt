package com.sportevent.app.data.local

import androidx.room.*
import com.sportevent.app.data.model.CountryModel

@Dao
interface CountryDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(country: CountryModel)

    @Delete
    fun delete(country: CountryModel)

    @Update
    fun update(country: CountryModel)

    @Query("SELECT * FROM countries WHERE id = :id")
    fun getById(id: Int): CountryModel

    @Query("SELECT * FROM countries")
    fun getAll(): List<CountryModel>
}