package com.sportevent.app.data.model;

public class ChatRoomModel {

    public ChatRoomModel(boolean isMyItem, String text) {
        this.isMyItem = isMyItem;
        this.text = text;
    }

    private boolean isMyItem;
    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isMyItem() {
        return isMyItem;
    }

    public void setMyItem(boolean myItem) {
        isMyItem = myItem;
    }
}
