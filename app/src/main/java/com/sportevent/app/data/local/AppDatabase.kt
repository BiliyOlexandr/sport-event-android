package com.sportevent.app.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.sportevent.app.data.model.CountryModel
import com.sportevent.app.data.model.SportKindModel
import com.sportevent.app.data.model.TypeEventModel
import com.sportevent.app.data.model.TypeParticipantModel


@Database(entities = [CountryModel::class, SportKindModel::class, TypeEventModel::class, TypeParticipantModel::class], version = 1, exportSchema = false)

abstract class AppDatabase : RoomDatabase() {
    abstract fun countryDao(): CountryDao
    abstract fun sportKindDao(): SportKindDao
    abstract fun typeEventDao(): TypeEventDao
    abstract fun typeParticipantDao(): TypeParticipantDao
}