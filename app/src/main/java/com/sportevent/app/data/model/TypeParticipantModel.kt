package com.sportevent.app.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "type_participant")
open class TypeParticipantModel {

    @PrimaryKey
    @SerializedName("id")
    var id: Int? = null

    @SerializedName("age_min")
    var ageMin: Int? = null

    @SerializedName("age_max")
    var ageMax: Int? = null

    @SerializedName("name")
    var name: String? = null
}