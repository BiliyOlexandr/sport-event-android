package com.sportevent.app.data.model

import com.google.gson.annotations.SerializedName

class CityResponse {

    @SerializedName("city")
    var city: CityModel? = null

    @SerializedName("country")
    var country: CountryModel? = null

    @SerializedName("status")
    var status: Int? = null

    @SerializedName("comment")
    var comment: String? = null
}