package com.sportevent.app.data.model

import com.google.gson.annotations.SerializedName

class UserModel {

    @SerializedName("id")
    var id: Int? = null

    @SerializedName("name")
    var name: String? = null

    @SerializedName("email")
    var emails: String? = null

    @SerializedName("lang")
    var lang: String? = null

    @SerializedName("country_id")
    var countryId: Int? = null

    @SerializedName("is_command")
    var isCommand: Int? = null

    @SerializedName("city_id")
    var cityId: Int? = null

    @SerializedName("phone")
    var phone: String? = null

    @SerializedName("icon")
    var icon: String? = null

    @SerializedName("event_active")
    var eventActive: Int? = null
}