package com.sportevent.app.data.model;

public class FilterModel {
    public int id;

    public FilterModel(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
