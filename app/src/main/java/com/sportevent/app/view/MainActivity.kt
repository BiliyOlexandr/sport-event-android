package com.sportevent.app.view

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import android.widget.Toast

import androidx.annotation.IdRes
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.KodeinAppCompatActivity
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance

import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.iid.FirebaseInstanceId
import com.pixplicity.easyprefs.library.Prefs
import com.sportevent.app.R
import com.sportevent.app.data.local.AppDatabase
import com.sportevent.app.data.model.SignUpResponse
import com.sportevent.app.service.APIService
import com.vk.sdk.VKAccessToken
import com.vk.sdk.VKCallback
import com.vk.sdk.VKSdk
import com.vk.sdk.api.VKError
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MainActivity : KodeinAppCompatActivity() {

    protected val kodein = LazyKodein(appKodein)
    protected val apiPublicService = kodein.instance<APIService>()
    protected val db = kodein.instance<AppDatabase>()

    private var changes: HashMap<String, Any?>? = null
    private var disposable: Disposable? = null

    private var bottomNavigationView: BottomNavigationView? = null
    private var navHostFragment: NavHostFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        changes = HashMap()

        navHostFragment = supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?

        val inflater = navHostFragment!!.navController.navInflater
        val graph = inflater.inflate(R.navigation.nav_graph)

        if (Prefs.getString("token", null) != null) {
            graph.startDestination = R.id.fragmentEvents
        } else {
            graph.startDestination = R.id.splashFragment
        }
        navHostFragment!!.navController.graph = graph

        bottomNavigationView = findViewById(R.id.bottomNavigation)
        NavigationUI.setupWithNavController(bottomNavigationView!!, navHostFragment!!.navController)

        showBadge(this, bottomNavigationView!!, R.id.fragmentMessages, "4")
        showBadge(this, bottomNavigationView!!, R.id.fragmentRequests, "2")

        navHostFragment!!.navController.addOnDestinationChangedListener { controller, destination, arguments ->
            when (destination.id) {
                R.id.splashFragment, R.id.signInFragment, R.id.signUpFragment, R.id.restorePassFragment -> isShowBottomNavigation(false)
                else -> isShowBottomNavigation(true)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!VKSdk.onActivityResult(requestCode, resultCode, data, object : VKCallback<VKAccessToken> {
                    override fun onResult(res: VKAccessToken) {

                        if (res.email == null){
                            Toast.makeText(this@MainActivity, "На Вашем аккаунте отсутствует почта, добавьте почту и повторите попытку!", Toast.LENGTH_SHORT).show()
                            return
                        }

                        val progressDialog = ProgressDialog(this@MainActivity)
                        progressDialog.setMessage("Вход с помощью vk+...")
                        progressDialog.show()

                        changes?.clear()
                        changes?.put("vk_user_id", res.userId)
                        changes?.put("email", res.email)
                        changes?.put("lang", "RU")
                        changes?.put("country_id", selectedCountryId)
                        changes?.put("device_token", FirebaseInstanceId.getInstance().token)
                        changes?.put("device", 0)

                        disposable?.dispose()
                        disposable = apiPublicService.value.vkSignIn(changes)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({ t: SignUpResponse ->

                                    if (t.status == 200){
                                        Prefs.putString("token", t.token)

                                        if (isForSignIn){
                                            navHostFragment?.navController?.navigate(R.id.action_signInFragment_to_fragmentEvents)
                                        } else {
                                            navHostFragment?.navController?.navigate(R.id.action_signUpFragment_to_fragmentEvents)
                                        }


                                    } else {
                                        Toast.makeText(this@MainActivity, t.comment, Toast.LENGTH_SHORT).show()
                                    }

                                    progressDialog.dismiss()

                                }, { e ->
                                    e.printStackTrace()
                                    progressDialog.dismiss()
                                })


                    }

                    override fun onError(error: VKError) {
                        Log.e(TAG, error.toString())
                    }
                })) {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onNavigateUp(): Boolean {
        return navHostFragment!!.navController.navigateUp()
    }

    private fun isShowBottomNavigation(isShow: Boolean) {
        if (isShow) {
            bottomNavigationView!!.visibility = View.VISIBLE
        } else {
            bottomNavigationView!!.visibility = View.GONE
        }
    }

    private fun showBadge(context: Context, bottomNavigationView: BottomNavigationView, @IdRes itemId: Int, value: String) {
        //        removeBadge(bottomNavigationView, itemId);
        val itemView = bottomNavigationView.findViewById<BottomNavigationItemView>(itemId)
        val badge = LayoutInflater.from(context).inflate(R.layout.notification_badge, bottomNavigationView, false)

        val text = badge.findViewById<TextView>(R.id.badge_text_view)
        text.text = value
        itemView.addView(badge)
    }

    companion object{
        var TAG = "MainActivity"
        var selectedCountryId = 0
        var isForSignIn = false
    }
}
