package com.sportevent.app.view.main.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.sportevent.app.R;
import com.sportevent.app.view.BaseFragment;
import com.sportevent.app.view.views.RichBottomNavigationView;

public class MainFragment extends BaseFragment implements RichBottomNavigationView.OnNavigationItemSelectedListener{

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        replaceChildFragment(new EventsFragment());

        RichBottomNavigationView navigationView = view.findViewById(R.id.bottomNavigation);
        navigationView.setOnNavigationItemSelectedListener(this);

        showBadge(getContext(), navigationView, R.id.fragmentMessages, "4");
        showBadge(getContext(), navigationView, R.id.fragmentRequests, "2");
    }

    private static void showBadge(Context context, BottomNavigationView bottomNavigationView, @IdRes int itemId, String value) {
        removeBadge(bottomNavigationView, itemId);
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        View badge = LayoutInflater.from(context).inflate(R.layout.notification_badge, bottomNavigationView, false);

        TextView text = badge.findViewById(R.id.badge_text_view);
        text.setText(value);
        itemView.addView(badge);
    }

    private static void removeBadge(BottomNavigationView bottomNavigationView, @IdRes int itemId) {
        BottomNavigationItemView itemView = bottomNavigationView.findViewById(itemId);
        if (itemView.getChildCount() == 3) {
            itemView.removeViewAt(2);
        }
    }

    private void displaySelectedScreen(int itemId) {

        switch (itemId) {
            case R.id.fragmentEvents:
//                replaceChildFragment(new EventsFragment());
                break;
            case R.id.fragmentMessages:
//                replaceChildFragment(new ChatFragment());
                break;
            case R.id.fragmentRequests:
//                replaceChildFragment(new RequestsFragment());
                break;
            case R.id.fragmentProfile:
//                replaceChildFragment(new ProfileFragment());
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        displaySelectedScreen(menuItem.getItemId());
        return true;
    }
}
