package com.sportevent.app.view.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.adapter.ParticipantsAdapter;
import com.sportevent.app.data.model.RequestModel;
import com.sportevent.app.view.BaseFragment;

import java.util.ArrayList;

public class ParticipantsFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.participants_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<RequestModel> chatModels = new ArrayList<>();
        chatModels.add(new RequestModel("Савенко Ярослава", "г. Абдулино", "СЯ"));
        chatModels.add(new RequestModel("Иван Иванов", "г. Данилов", "ИИ"));
        chatModels.add(new RequestModel("Горобчук Устинья", "г. Азов", "ГУ"));
        chatModels.add(new RequestModel("Константин Константичнович", "г. Анапа", "КК"));
        chatModels.add(new RequestModel("Евдокимов Устин", "г. Питербург", "ЕУ"));
        chatModels.add(new RequestModel("Уварова Нина", "г. Омск", "УН"));

        chatModels.add(new RequestModel("Белый Дракон", "г. Данилов", "БД"));
        chatModels.add(new RequestModel("Спартак", "г. Данилов", "СП"));
        chatModels.add(new RequestModel("Синий Тигр", "г. Данилов", "СТ"));

        RecyclerView rvChat = view.findViewById(R.id.rvParticipants);
        rvChat.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvChat.hasFixedSize();
        rvChat.setAdapter(new ParticipantsAdapter(chatModels, new ParticipantsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(RequestModel item) {

            }
        }));
    }
}
