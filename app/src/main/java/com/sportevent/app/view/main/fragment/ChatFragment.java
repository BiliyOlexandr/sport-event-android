package com.sportevent.app.view.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.adapter.ChatAdapter;
import com.sportevent.app.data.model.ChatModel;
import com.sportevent.app.view.BaseFragment;

import java.util.ArrayList;

public class ChatFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.chat_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<ChatModel> chatModels = new ArrayList<>();
        chatModels.add(new ChatModel(true, 4));
        chatModels.add(new ChatModel(false, 0));

        RecyclerView rvChat = view.findViewById(R.id.rvChat);
        rvChat.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvChat.hasFixedSize();
        rvChat.setAdapter(new ChatAdapter(chatModels, new ChatAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ChatModel item) {
                Bundle bundle = new Bundle();
                bundle.putString("arg_title", "Константин Константинович");
                bundle.putString("arg_event", "Мастер класс по игре в шахматы");
                Navigation.findNavController(view).navigate(R.id.chatRoomFragment, bundle);
            }
        }));
    }
}
