package com.sportevent.app.view.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.adapter.ReportAdapter;
import com.sportevent.app.data.model.ReportModel;
import com.sportevent.app.view.BaseFragment;

import java.util.ArrayList;

public class ReportFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.report_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_ios_24px);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack(view);
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        toolbarTitle.setText("Пожаловаться");

        ArrayList<ReportModel> eventOrganizerModels = new ArrayList<>();
        eventOrganizerModels.add(new ReportModel("Ложная информация"));
        eventOrganizerModels.add(new ReportModel("Мошенничество"));
        eventOrganizerModels.add(new ReportModel("Причина 1"));
        eventOrganizerModels.add(new ReportModel("Причина 2"));
        eventOrganizerModels.add(new ReportModel("Причина 3"));

        RecyclerView rvUsers = view.findViewById(R.id.rvItems);
        rvUsers.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvUsers.hasFixedSize();
        rvUsers.setAdapter(new ReportAdapter(eventOrganizerModels, new ReportAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ReportModel item) {
                Bundle bundle = new Bundle();
                bundle.putString("arg_title", item.getTitle());
                Navigation.findNavController(view).navigate(R.id.action_reportFragment_to_reportCreatingFragment, bundle);
            }
        }));
    }
}
