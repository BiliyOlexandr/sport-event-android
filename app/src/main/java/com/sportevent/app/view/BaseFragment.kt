package com.sportevent.app.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.github.salomonbrys.kodein.LazyKodein
import com.github.salomonbrys.kodein.android.appKodein
import com.github.salomonbrys.kodein.instance
import com.sportevent.app.data.local.AppDatabase
import com.sportevent.app.service.APIService

abstract class BaseFragment : Fragment() {

    protected val kodein = LazyKodein(appKodein)
    protected val apiService = kodein.instance<APIService>()
    protected val db = kodein.instance<AppDatabase>()

    protected lateinit var mainActivity: MainActivity

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mainActivity = activity as MainActivity
    }

    //
    //    public void replaceFragment(Fragment fragment){
    //        FragmentTransaction ft = mainActivity.getSupportFragmentManager().beginTransaction();
    //        ft.replace(R.id.container, fragment);
    //        ft.addToBackStack(null);
    //        ft.commitAllowingStateLoss();
    //    }

    fun goBack(view: View) {
        Navigation.findNavController(view).navigateUp()
        //        if (mainActivity.getSupportFragmentManager().getBackStackEntryCount() > 0){
        //            mainActivity.getSupportFragmentManager().popBackStackImmediate();
        //        }
    }

    fun goBack() {
        findNavController().navigateUp()
    }
}
