package com.sportevent.app.view.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.adapter.ChatRoomAdapter;
import com.sportevent.app.data.model.ChatRoomModel;
import com.sportevent.app.view.BaseFragment;

import java.util.ArrayList;

public class ChatRoomFragment extends BaseFragment {

    private static final String ARG_TITLE = "arg_title";
    private static final String ARG_EVENT = "arg_event";
    private ChatRoomAdapter chatRoomAdapter;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.chat_room_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_ios_24px);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack(view);
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);

        LinearLayout eventLayout = view.findViewById(R.id.eventLayout);
        final TextView eventName = view.findViewById(R.id.eventName);
        TextView count = view.findViewById(R.id.count);

        if (getArguments() != null){
            toolbarTitle.setText(getArguments().getString(ARG_TITLE));
            if (getArguments().getString(ARG_EVENT) != null){
                eventLayout.setVisibility(View.VISIBLE);
                eventName.setText(getArguments().getString(ARG_EVENT));
            } else {
                count.setVisibility(View.GONE);
                eventLayout.setVisibility(View.GONE);
            }
        }

        eventLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("arg_title", eventName.getText().toString());
                Navigation.findNavController(view).navigate(R.id.action_chatRoomFragment_to_eventInfoFragment, bundle);
            }
        });

        ArrayList<ChatRoomModel> chatModels = new ArrayList<>();
        chatModels.add(new ChatRoomModel(false, "Здравствуйте! Какие условия записи к Вам на тренировки?"));
        chatModels.add(new ChatRoomModel(true, "Добрый день! Самые лучшие)))"));


        RecyclerView rvRequests = view.findViewById(R.id.rvRequests);
        rvRequests.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvRequests.hasFixedSize();
        chatRoomAdapter = new ChatRoomAdapter(chatModels);
        rvRequests.setAdapter(chatRoomAdapter);

//        final EditText messageText = view.findViewById(R.id.messageText);
//
//        FrameLayout sendBtn = view.findViewById(R.id.sendBtn);
//        sendBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (messageText.getText().length() > 0){
//                    chatRoomAdapter.addItem(new ChatRoomModel(true, messageText.getText().toString().trim()));
//                } else {
//                    return;
//                }
//
//            }
//        });

    }

    public static ChatRoomFragment newInstance(String title, String event) {

        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        args.putString(ARG_EVENT, event);
        ChatRoomFragment fragment = new ChatRoomFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
