package com.sportevent.app.view.registration.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.cardview.widget.CardView
import androidx.navigation.fragment.findNavController

import com.sportevent.app.R
import com.sportevent.app.data.local.CountryDao
import com.sportevent.app.data.model.CountriesResponse
import com.sportevent.app.view.BaseFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class SplashFragment : BaseFragment() {

    private var disposable: Disposable? = null
    private lateinit var countryDao: CountryDao

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.splash_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        countryDao = db.value.countryDao()

        getCountries()

        val signUpBnt = view.findViewById<CardView>(R.id.signUpBnt)
        val signInBtn = view.findViewById<CardView>(R.id.signInBtn)

        signUpBnt.setOnClickListener { findNavController().navigate(R.id.action_splashFragment_to_signUpFragment) }

        signInBtn.setOnClickListener { findNavController().navigate(R.id.action_splashFragment_to_signInFragment) }
    }

    private fun getCountries(){

        disposable?.dispose()
        disposable = apiService.value.getCountriesList("RU")
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: CountriesResponse ->

                    if (t.status == 200){
                        if (t.countries != null){
                            for (i in t.countries!!.indices){
                                countryDao.insert(t.countries!![i])
                            }
                        }
                    } else {
                        Toast.makeText(context, t.comment, Toast.LENGTH_SHORT).show()
                    }

                }, { e ->
                    e.printStackTrace()
                })
    }
}
