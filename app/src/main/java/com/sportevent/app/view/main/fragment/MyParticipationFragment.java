package com.sportevent.app.view.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.adapter.EventAdapter;
import com.sportevent.app.data.model.EventsModel;
import com.sportevent.app.view.BaseFragment;

import java.util.ArrayList;

public class MyParticipationFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.my_participation_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<EventsModel> chatModels = new ArrayList<>();

        RecyclerView rvChat = view.findViewById(R.id.rvParticipants);
        rvChat.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvChat.hasFixedSize();

        chatModels.add(new EventsModel());
        chatModels.add(new EventsModel());
        chatModels.add(new EventsModel());

        rvChat.setAdapter(new EventAdapter(chatModels, new EventAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(EventsModel item) {
                Bundle bundle = new Bundle();
                bundle.putString("arg_title", "Sochi Hockey Open");
                Navigation.findNavController(view).navigate(R.id.eventInfoFragment, bundle);
            }
        }));
    }
}
