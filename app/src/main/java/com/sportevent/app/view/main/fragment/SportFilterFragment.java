package com.sportevent.app.view.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.adapter.SportFilterAdapter;
import com.sportevent.app.data.model.FilterModel;
import com.sportevent.app.view.BaseFragment;

import java.util.ArrayList;

public class SportFilterFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.sport_filter_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_ios_24px);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack(view);
            }
        });

        ArrayList<FilterModel> chatModels = new ArrayList<>();
        chatModels.add(new FilterModel(1));
        chatModels.add(new FilterModel(2));
        chatModels.add(new FilterModel(3));
        chatModels.add(new FilterModel(4));
        chatModels.add(new FilterModel(5));
        chatModels.add(new FilterModel(6));
        chatModels.add(new FilterModel(7));
        chatModels.add(new FilterModel(8));
        chatModels.add(new FilterModel(9));
        chatModels.add(new FilterModel(10));
        chatModels.add(new FilterModel(11));
        chatModels.add(new FilterModel(12));
        chatModels.add(new FilterModel(13));

        RecyclerView rvChat = view.findViewById(R.id.rvChat);
        rvChat.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvChat.hasFixedSize();
        rvChat.setAdapter(new SportFilterAdapter(chatModels));
    }
}
