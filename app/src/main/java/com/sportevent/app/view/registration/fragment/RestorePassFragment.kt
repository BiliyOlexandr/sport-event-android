package com.sportevent.app.view.registration.fragment

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.sportevent.app.R
import com.sportevent.app.data.model.SimpleResponse
import com.sportevent.app.view.BaseFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.restore_pass_fragment.*

class RestorePassFragment : BaseFragment() {

    private var disposable: Disposable? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.restore_pass_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_ios_24px)
        toolbar.setNavigationOnClickListener {   goBack() }

        restoreBtn.setOnClickListener {

            if (emailField.text.isNullOrEmpty()){
                Toast.makeText(context, "Введите e-mail", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!emailField.text.toString().contains("@")){
                Toast.makeText(context, "Не валидный e-mail", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val progressDialog = ProgressDialog(context)
            progressDialog.setMessage("Сброс пароля...")
            progressDialog.show()

            disposable?.dispose()
            disposable = apiService.value.resporePassword(emailField.text.toString().trim())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: SimpleResponse ->

                        if (t.status == 200){
                            Toast.makeText(context, "На ваш email отправлено письмо для восстановления пароля.", Toast.LENGTH_SHORT).show()
                            findNavController().navigate(R.id.action_restorePassFragment_to_splashFragment)
                        } else {
                            Toast.makeText(context, t.comment, Toast.LENGTH_SHORT).show()
                        }

                        progressDialog.dismiss()

                    }, { e ->
                        e.printStackTrace()
                        progressDialog.dismiss()
                    })

        }
    }
}
