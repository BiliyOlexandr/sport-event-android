package com.sportevent.app.view.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.adapter.FeedbackAdapter;
import com.sportevent.app.data.model.Feedback;
import com.sportevent.app.view.BaseFragment;

import java.util.ArrayList;

public class FeedbacksFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.feedbacks_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<Feedback> chatModels = new ArrayList<>();
        chatModels.add(new Feedback());
        chatModels.add(new Feedback());

        RecyclerView rvChat = view.findViewById(R.id.rvParticipants);
        rvChat.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvChat.hasFixedSize();
        rvChat.setAdapter(new FeedbackAdapter(chatModels, new FeedbackAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Feedback item) {

            }
        }));
    }
}
