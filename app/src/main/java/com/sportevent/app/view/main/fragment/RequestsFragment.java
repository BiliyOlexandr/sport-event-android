package com.sportevent.app.view.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.adapter.RequestsAdapter;
import com.sportevent.app.data.model.Feedback;
import com.sportevent.app.view.BaseFragment;

import java.util.ArrayList;

public class RequestsFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.requests_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<Feedback> requestModels = new ArrayList<>();
        requestModels.add(new Feedback());

        RecyclerView rvRequests = view.findViewById(R.id.rvRequests);
        rvRequests.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvRequests.hasFixedSize();
        rvRequests.setAdapter(new RequestsAdapter(requestModels, new RequestsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Feedback item) {
                Bundle bundle = new Bundle();
                bundle.putString("arg_title", "Кубок Девиса");
                Navigation.findNavController(view).navigate(R.id.action_fragmentRequests_to_requestDetailsFragment);
            }
        }));
    }
}
