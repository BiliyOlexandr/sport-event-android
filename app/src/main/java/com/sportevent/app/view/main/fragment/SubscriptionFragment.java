package com.sportevent.app.view.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.adapter.FollowersAdapter;
import com.sportevent.app.data.model.FollowerModel;
import com.sportevent.app.view.BaseFragment;

import java.util.ArrayList;

public class SubscriptionFragment extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.feedbacks_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ArrayList<FollowerModel> chatModels = new ArrayList<>();

        RecyclerView rvChat = view.findViewById(R.id.rvParticipants);
        rvChat.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvChat.hasFixedSize();

        chatModels.add(new FollowerModel(false));
        chatModels.add(new FollowerModel(true));
        chatModels.add(new FollowerModel(false));

        rvChat.setAdapter(new FollowersAdapter(chatModels, new FollowersAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(FollowerModel item) {
                Bundle bundle = new Bundle();
                bundle.putString("arg_title", "Мария Климова");
                Navigation.findNavController(view).navigate(R.id.userProfileFragment, bundle);
            }
        }));
    }
}
