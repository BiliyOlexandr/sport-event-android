package com.sportevent.app.view.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.navigation.Navigation;

import com.sportevent.app.R;
import com.sportevent.app.view.BaseFragment;

public class EventAddRequestFragment extends BaseFragment {

    private static final String ARG_TITLE = "arg_title";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.event_add_request_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.event_menu);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_ios_24px);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack(view);
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        if (getArguments() != null){
            toolbarTitle.setText(getArguments().getString(ARG_TITLE));
        }

        final CardView sendBtn = view.findViewById(R.id.sendBtn);
        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("arg_title", "Sochi Hockey Open");
                Navigation.findNavController(view).navigate(R.id.action_eventAddRequestFragment_to_eventRequestConfirmationFragment, bundle);
            }
        });
    }
}
