package com.sportevent.app.view.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sportevent.app.R;
import com.sportevent.app.adapter.RequestDetailsAdapter;
import com.sportevent.app.data.model.Feedback;
import com.sportevent.app.view.BaseFragment;

import java.util.ArrayList;

public class RequestDetailsFragment extends BaseFragment {

    private static final String ARG_TITLE = "arg_title";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.request_details_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_ios_24px);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack(view);
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        if (getArguments() != null){
            toolbarTitle.setText(getArguments().getString(ARG_TITLE));
        }

        ArrayList<Feedback> requestModels = new ArrayList<>();
        requestModels.add(new Feedback());
        requestModels.add(new Feedback());

        RecyclerView rvRequests = view.findViewById(R.id.rvRequests);
        rvRequests.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.VERTICAL, false));
        rvRequests.hasFixedSize();
        rvRequests.setAdapter(new RequestDetailsAdapter(requestModels, new RequestDetailsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Feedback item) {
                Toast.makeText(getContext(), "Item clicked", Toast.LENGTH_LONG).show();
            }
        }));
    }

    public static RequestDetailsFragment newInstance(String title) {

        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        RequestDetailsFragment fragment = new RequestDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
