package com.sportevent.app.view.registration.fragment

import android.Manifest
import android.app.ProgressDialog
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.navigation.fragment.findNavController

import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.iid.FirebaseInstanceId
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.pixplicity.easyprefs.library.Prefs
import com.sportevent.app.R
import com.sportevent.app.data.local.CountryDao
import com.sportevent.app.data.model.CityResponse
import com.sportevent.app.data.model.CountriesResponse
import com.sportevent.app.data.model.CountryModel
import com.sportevent.app.data.model.SignUpResponse
import com.sportevent.app.view.BaseFragment
import com.sportevent.app.view.MainActivity
import com.vk.sdk.VKScope
import com.vk.sdk.VKSdk
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.sign_up_fragment.*

class SignUpFragment : BaseFragment(), GoogleApiClient.OnConnectionFailedListener {

    private var disposable: Disposable? = null
    private var changes: HashMap<String, Any?>? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var callbackManager: CallbackManager? = null
    private var loginButton: LoginButton? = null
    private val scope = arrayOf(VKScope.WALL, VKScope.EMAIL)
    private var mAuth: FirebaseAuth? = null
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private var selectedCountyId: Int = 1
    private lateinit var countriesList: List<CountryModel>

    private lateinit var countryDao: CountryDao
    private lateinit var imm: InputMethodManager

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        imm = mainActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        Log.e(TAG, "onActivityResult: 1")
        if (requestCode == RC_SIGN_IN) {
            Log.e(TAG, "onActivityResult: 2")
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            val acct = result.signInAccount
            firebaseAuthWithGoogle(acct!!)
            //String token = acct.getIdToken();
            //Log.d("google token", token);
        }
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.sign_up_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        countryDao = db.value.countryDao()

        changes = HashMap()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)

        val manager = context?.getSystemService(LOCATION_SERVICE ) as LocationManager
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){

            if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                getLocationPermission()

            } else {
                fusedLocationClient.lastLocation.addOnSuccessListener {

                    if (it != null){
                        getInfoByLocation(it.latitude, it.longitude)
                    } else {
                        getCountries()
                    }
                }
            }

        } else {
            getCountries()
            Toast.makeText(context, "GPS disabled", Toast.LENGTH_SHORT).show()
        }

        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        val fbBtn = view.findViewById<ImageView>(R.id.fbBtn)
        val vkBtn = view.findViewById<ImageView>(R.id.vkBtn)
        val gpBtn = view.findViewById<ImageView>(R.id.gpBtn)
        val signUpBtn = view.findViewById<CardView>(R.id.signUpBtn)
        loginButton = view.findViewById(R.id.login_button)

        callbackManager = CallbackManager.Factory.create()
        loginButton!!.setPermissions("email", "public_profile")
        loginButton!!.fragment = this
        loginButton!!.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {

                val progressDialog = ProgressDialog(context)
                progressDialog.setMessage("Вход с помощью facebook+...")
                progressDialog.show()

                changes?.clear()
                changes?.put("fb_token", loginResult.accessToken.token)
                changes?.put("lang", "RU")
                changes?.put("country_id", selectedCountyId)
                changes?.put("device_token", FirebaseInstanceId.getInstance().token)
                changes?.put("device", 0)

                disposable?.dispose()
                disposable = apiService.value.fbSignIn(changes)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ t: SignUpResponse ->

                            if (t.status == 200){
                                Prefs.putString("token", t.token)
                                findNavController().navigate(R.id.action_signUpFragment_to_fragmentEvents)
                            } else {
                                Toast.makeText(context, t.comment, Toast.LENGTH_SHORT).show()
                            }

                            progressDialog.dismiss()

                        }, { e ->
                            e.printStackTrace()
                            progressDialog.dismiss()
                        })

            }

            override fun onCancel() {

            }

            override fun onError(exception: FacebookException) {
                Log.e(TAG, exception.toString())
            }
        })
        mAuth = FirebaseAuth.getInstance()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        mGoogleSignInClient = GoogleSignIn.getClient(activity!!, gso)

        signUpBtn.setOnClickListener {

            if (nameField.text.isNullOrEmpty()){
                Toast.makeText(context, "Введите имя", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (emailField.text.isNullOrEmpty()){
                Toast.makeText(context, "Введите e-mail", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!emailField.text.toString().contains("@")){
                Toast.makeText(context, "Некорректный адрес электронной почты", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (passwordField.text.isNullOrEmpty()){
                Toast.makeText(context, "Введите пароль", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (countryField.text.isNullOrEmpty()){
                Toast.makeText(context, "Выберите страну", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            imm.hideSoftInputFromWindow(getView()!!.windowToken, 0)

            val progressDialog = ProgressDialog(context)
            progressDialog.setMessage("Регистрация пользователя...")
            progressDialog.show()

            changes?.clear()

            changes?.put("name", nameField.text.toString().trim())
            changes?.put("email", emailField.text.toString().trim())
            changes?.put("pass", passwordField.text.toString().trim())
            changes?.put("lang", "RU")
            changes?.put("country_id", selectedCountyId)
            changes?.put("device_token", FirebaseInstanceId.getInstance().token)
            changes?.put("device", 0)

            disposable?.dispose()
            disposable = apiService.value.signUpSimple(changes)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: SignUpResponse ->

                        if (t.status == 200){
                            Prefs.putString("token", t.token)
                            findNavController().navigate(R.id.action_signUpFragment_to_fragmentEvents)
                        } else {
                            Toast.makeText(context, t.comment, Toast.LENGTH_SHORT).show()
                        }

                        progressDialog.dismiss()

                    }, { e ->
                        e.printStackTrace()
                        progressDialog.dismiss()
                    })

        }

        countryField.setOnClickListener {
            showDialog(context!!, countriesList, "Выберите страну")
        }

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_ios_24px)
        toolbar.setNavigationOnClickListener { goBack() }

        gpBtn.setOnClickListener {

            if (selectedCountyId != null){
                val signInIntent = mGoogleSignInClient.signInIntent
                startActivityForResult(signInIntent, RC_SIGN_IN)
            } else {
                Toast.makeText(context, "Выберите страну", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

        }

        fbBtn.setOnClickListener {
            if (selectedCountyId != null){
                loginButton!!.performClick()
            } else {
                Toast.makeText(context, "Выберите страну", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
        }

        vkBtn.setOnClickListener {
            if (selectedCountyId != null){
                MainActivity.isForSignIn = false
                VKSdk.login(activity!!, *scope)
            } else {
                Toast.makeText(context, "Выберите страну", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
        }
    }


    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.id!!)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        mAuth!!.signInWithCredential(credential)
                .addOnCompleteListener(activity!!) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success")
                        val user = mAuth!!.currentUser
                        val uid = user!!.uid // Вот эту херь отправляй

                        val progressDialog = ProgressDialog(context)
                        progressDialog.setMessage("Вход с помощью google+...")
                        progressDialog.show()

                        changes?.clear()
                        changes?.put("firebase_user_id", uid)
                        changes?.put("lang", "RU")
                        changes?.put("country_id", selectedCountyId)
                        changes?.put("device_token", FirebaseInstanceId.getInstance().token)
                        changes?.put("device", 0)

                        disposable?.dispose()
                        disposable = apiService.value.googleSignIn(changes)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({ t: SignUpResponse ->

                                    if (t.status == 200){
                                        Prefs.putString("token", t.token)
                                        findNavController().navigate(R.id.action_signUpFragment_to_fragmentEvents)
                                    } else {
                                        Toast.makeText(context, t.comment, Toast.LENGTH_SHORT).show()
                                    }

                                    progressDialog.dismiss()

                                }, { e ->
                                    e.printStackTrace()
                                    progressDialog.dismiss()
                                })

                    } else {
                        Log.w(TAG, "signInWithCredential:failure", task.exception)
                    }
                }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.e("gp login", connectionResult.toString())
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        if (result.isSuccess) {
            println(result.signInAccount!!.idToken)
        }
    }

    private fun getLocationPermission(){
        Dexter.withActivity(activity)
                .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(object : MultiplePermissionsListener{
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted()){
                            fusedLocationClient.lastLocation.addOnSuccessListener {
                                if (it != null){
                                    getInfoByLocation(it.latitude, it.longitude)
                                } else {
                                    getCountries()
                                }
                            }

                        } else {
                            getCountries()
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        token?.continuePermissionRequest()
                    }

                })
                .check()
    }

    private fun getInfoByLocation(latitude: Double, longitude: Double){

        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Определение страны...")
        progressDialog.show()

        changes?.clear()
        changes?.put("lang", "RU")
        changes?.put("lng", longitude)
        changes?.put("lat", latitude)

        disposable?.dispose()
        disposable = apiService.value.getCityByGeo(changes)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: CityResponse ->

                    if (t.status == 200){

                        countryField.setText(t.country?.name)
                        selectedCountyId = t.country?.id!!
                        MainActivity.selectedCountryId = t.country?.id!!
                        countryField.isEnabled = false

                    } else {
                        Toast.makeText(context, t.comment, Toast.LENGTH_SHORT).show()
                        getCountries()
                    }

                    progressDialog.dismiss()

                }, { e ->
                    e.printStackTrace()
                    progressDialog.dismiss()
                    getCountries()
                })
    }

    private fun getCountries(){

        if (countryDao.getAll().isNullOrEmpty()){
            disposable?.dispose()
            disposable = apiService.value.getCountriesList("RU")
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: CountriesResponse ->

                        if (t.status == 200){
                            if (t.countries != null){
                                countriesList = t.countries!!
                                for (i in t.countries!!.indices){
                                    countryDao.insert(t.countries!![i])
                                }
                            }
                        } else {
                            Toast.makeText(context, t.comment, Toast.LENGTH_SHORT).show()
                        }

                    }, { e ->
                        e.printStackTrace()
                    })

        } else {
            countriesList = countryDao.getAll()
        }


    }

    private fun showDialog(context: Context, list: List<CountryModel>, title: String){
        val titleList = arrayListOf<String>()
        for (i in list.indices){
            titleList.add(list[i].name!!)
        }
        val items = toArray<String>(titleList)
        val builder = AlertDialog.Builder(context)
        with(builder) {

            setTitle(title)

            setItems(items) { dialog, which ->

                selectedCountyId = list[which].id!!
                MainActivity.selectedCountryId = list[which].id!!
                countryField.setText(list[which].name)
            }

            setNegativeButton("Отмена") { dialog, which ->
                dialog.dismiss()
            }
            show()
        }
    }

    inline fun <reified T> toArray(list: List<*>): Array<T> {
        return (list as List<T>).toTypedArray()
    }

    companion object {
        private var TAG = "SignUpFragment"
        private val RC_SIGN_IN = 1
    }
}
