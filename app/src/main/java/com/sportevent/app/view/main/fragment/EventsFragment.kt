package com.sportevent.app.view.main.fragment

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.pixplicity.easyprefs.library.Prefs
import com.sportevent.app.R
import com.sportevent.app.adapter.EventsAdapter
import com.sportevent.app.adapter.OrganizerAdapter
import com.sportevent.app.data.local.CountryDao
import com.sportevent.app.data.local.SportKindDao
import com.sportevent.app.data.local.TypeEventDao
import com.sportevent.app.data.local.TypeParticipantDao
import com.sportevent.app.data.model.CityResponse
import com.sportevent.app.data.model.DictionariesResponse
import com.sportevent.app.data.model.EventModel
import com.sportevent.app.data.model.EventOrganizerModel
import com.sportevent.app.view.BaseFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.events_fragment.*

import java.util.ArrayList

class EventsFragment : BaseFragment(), Toolbar.OnMenuItemClickListener {

    private var disposable: Disposable? = null
    private lateinit var countryDao: CountryDao
    private lateinit var sportKindDao: SportKindDao
    private lateinit var typeEventDao: TypeEventDao
    private lateinit var typeParticipantDao: TypeParticipantDao


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.events_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val toolbar = view.findViewById<Toolbar>(R.id.toolbar)
        toolbar.inflateMenu(R.menu.filter_menu)
        toolbar.setOnMenuItemClickListener(this)

        countryDao = db.value.countryDao()
        sportKindDao = db.value.sportKindDao()
        typeEventDao = db.value.typeEventDao()
        typeParticipantDao = db.value.typeParticipantDao()

        getDictionaries()

        val eventOrganizerModels = ArrayList<EventOrganizerModel>()
        eventOrganizerModels.add(EventOrganizerModel())
        eventOrganizerModels.add(EventOrganizerModel())
        eventOrganizerModels.add(EventOrganizerModel())
        eventOrganizerModels.add(EventOrganizerModel())
        eventOrganizerModels.add(EventOrganizerModel())
        eventOrganizerModels.add(EventOrganizerModel())
        eventOrganizerModels.add(EventOrganizerModel())
        eventOrganizerModels.add(EventOrganizerModel())

        rvUsers.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        rvUsers.hasFixedSize()
        rvUsers.adapter = OrganizerAdapter(eventOrganizerModels, OrganizerAdapter.OnItemClickListener {
            //                Bundle bundle = new Bundle();
            //                bundle.putString("arg_title", "Мария Климова");
            //                Navigation.findNavController(view).navigate(R.id.action_fragmentEvents_to_userProfileFragment, bundle);
        })

        val eventModels = ArrayList<EventModel>()
        eventModels.add(EventModel())
        eventModels.add(EventModel())
        eventModels.add(EventModel())
        eventModels.add(EventModel())
        eventModels.add(EventModel())

        rvItems.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
        rvItems.hasFixedSize()
        rvItems.adapter = EventsAdapter(eventModels, EventsAdapter.OnItemClickListener {
            val bundle = Bundle()
            bundle.putString("arg_title", "Sochi Hockey Open")
            findNavController().navigate(R.id.action_fragmentEvents_to_eventInfoFragment, bundle)
        }, EventsAdapter.OnItemClickListener2 {
            val bundle = Bundle()
            bundle.putString("arg_title", "Мария Климова")
            findNavController().navigate(R.id.action_fragmentEvents_to_userProfileFragment, bundle)
        })

        val fab = view.findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { findNavController().navigate(R.id.createEventGeneralFragment) }
    }

    override fun onMenuItemClick(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.actionFilter -> {
                findNavController().navigate(R.id.action_fragmentEvents_to_filterFragment)
                return true
            }
        }
        return false
    }

    private fun getDictionaries(){

        if (countryDao.getAll().isNullOrEmpty() || sportKindDao.getAll().isNullOrEmpty() || typeEventDao.getAll().isNullOrEmpty() || typeParticipantDao.getAll().isNullOrEmpty()){

            val progressDialog = ProgressDialog(context)
            progressDialog.setMessage("Загрузка информации...")
            progressDialog.show()

            disposable?.dispose()
            disposable = apiService.value.getDictionaries(Prefs.getString("token", null), "RU")
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: DictionariesResponse ->

                        if (t.status == 200){

                            val countries = t.country
                            for (i in countries!!.indices){
                                countryDao.insert(countries[i])
                            }

                            val sports = t.sportKkind
                            for (i in sports!!.indices){
                                sportKindDao.insert(sports[i])
                            }

                            val participants = t.typeParticipant
                            for (i in participants!!.indices){
                                typeParticipantDao.insert(participants[i])
                            }

                            val eventTypes = t.typeEvent
                            for (i in eventTypes!!.indices){
                                typeEventDao.insert(eventTypes[i])
                            }

                        } else {
                            Toast.makeText(context, t.comment, Toast.LENGTH_SHORT).show()
                        }

                        progressDialog.dismiss()
                    }, { e ->
                        e.printStackTrace()
                        progressDialog.dismiss()
                    })
        }
    }
}
