package com.sportevent.app.view.registration.fragment

import android.Manifest
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.iid.FirebaseInstanceId
import com.karumi.dexter.Dexter
import com.karumi.dexter.MultiplePermissionsReport
import com.karumi.dexter.PermissionToken
import com.karumi.dexter.listener.PermissionRequest
import com.karumi.dexter.listener.multi.MultiplePermissionsListener
import com.pixplicity.easyprefs.library.Prefs

import com.sportevent.app.R
import com.sportevent.app.data.model.CityResponse
import com.sportevent.app.data.model.SignUpResponse
import com.sportevent.app.view.BaseFragment
import com.sportevent.app.view.MainActivity
import com.sportevent.app.view.MainActivity.Companion.isForSignIn
import com.vk.sdk.VKScope
import com.vk.sdk.VKSdk
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.sign_in_fragment.*

class SignInFragment : BaseFragment(), GoogleApiClient.OnConnectionFailedListener {

    private var disposable: Disposable? = null
    private var changes: HashMap<String, Any?>? = null
    private var mAuth: FirebaseAuth? = null
    private var callbackManager: CallbackManager? = null
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var selectedCountyId: Int = 1
    private val scope = arrayOf(VKScope.WALL, VKScope.EMAIL)
    private lateinit var imm: InputMethodManager

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        imm = mainActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        Log.e(TAG, "onActivityResult: 1")
        if (requestCode == RC_SIGN_IN) {
            Log.e(TAG, "onActivityResult: 2")
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            val acct = result.signInAccount
            firebaseAuthWithGoogle(acct!!)
            //String token = acct.getIdToken();
            //Log.d("google token", token);
        }
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.sign_in_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_ios_24px)
        toolbar.setNavigationOnClickListener {  goBack() }

        changes = HashMap()

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)

        val manager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (manager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                getLocationPermission()

            } else {
                fusedLocationClient.lastLocation.addOnSuccessListener {
                    if (it != null){
                        getInfoByLocation(it.latitude, it.longitude)
                    } else {
                        MainActivity.selectedCountryId = 1
                        selectedCountyId = 1
                    }
                }
            }
        } else {
            MainActivity.selectedCountryId = 1
            selectedCountyId = 1
            Toast.makeText(context, "GPS disabled", Toast.LENGTH_SHORT).show()
        }



        callbackManager = CallbackManager.Factory.create()
        login_button?.setPermissions("email", "public_profile")
        login_button?.fragment = this
        login_button?.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {

                val progressDialog = ProgressDialog(context)
                progressDialog.setMessage("Вход с помощью facebook+...")
                progressDialog.show()

                changes?.clear()
                changes?.put("fb_token", loginResult.accessToken.token)
                changes?.put("lang", "RU")
                changes?.put("country_id", selectedCountyId)
                changes?.put("device_token", FirebaseInstanceId.getInstance().token)
                changes?.put("device", 0)

                disposable?.dispose()
                disposable = apiService.value.fbSignIn(changes)
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({ t: SignUpResponse ->

                            if (t.status == 200){
                                Prefs.putString("token", t.token)
                                findNavController().navigate(R.id.action_signInFragment_to_fragmentEvents)
                            } else {
                                Toast.makeText(context, t.comment, Toast.LENGTH_SHORT).show()
                            }

                            progressDialog.dismiss()

                        }, { e ->
                            e.printStackTrace()
                            progressDialog.dismiss()
                        })

            }

            override fun onCancel() {

            }

            override fun onError(exception: FacebookException) {
                Log.e(TAG, exception.toString())
            }
        })

        mAuth = FirebaseAuth.getInstance()
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        mGoogleSignInClient = GoogleSignIn.getClient(activity!!, gso)

        signInBtn.setOnClickListener{

            if (emailField.text.isNullOrEmpty()){
                Toast.makeText(context, "Введите e-mail", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (!emailField.text.toString().contains("@")){
                Toast.makeText(context, "Некорректный адрес электронной почты", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (passwordField.text.isNullOrEmpty()){
                Toast.makeText(context, "Введите пароль", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            imm.hideSoftInputFromWindow(getView()!!.windowToken, 0)

            changes?.clear()
            changes?.put("email", emailField.text.toString().trim())
            changes?.put("pass", passwordField.text.toString().trim())
            changes?.put("device_token", FirebaseInstanceId.getInstance().token)
            changes?.put("device", 0)

            val progressDialog = ProgressDialog(context)
            progressDialog.setMessage("Авторизация пользователя...")
            progressDialog.show()

            disposable?.dispose()
            disposable = apiService.value.signInSimple(changes)
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ t: SignUpResponse ->

                        if (t.status == 200){
                            Prefs.putString("token", t.token)
                            Navigation.findNavController(view).navigate(R.id.action_signInFragment_to_fragmentEvents)
                        } else {
                            Toast.makeText(context, t.comment, Toast.LENGTH_SHORT).show()
                        }

                        progressDialog.dismiss()

                    }, { e ->
                        e.printStackTrace()
                        progressDialog.dismiss()
                    })

        }

        fbBtn.setOnClickListener{
            login_button?.performClick()
        }

        vkBtn.setOnClickListener{
            isForSignIn = true
            VKSdk.login(activity!!, *scope)
        }

        gpBtn.setOnClickListener{
            val signInIntent = mGoogleSignInClient.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }

        textForgot.setOnClickListener{
            findNavController().navigate(R.id.action_signInFragment_to_restorePassFragment)
        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.id!!)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        mAuth!!.signInWithCredential(credential)
                .addOnCompleteListener(activity!!) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success")
                        val user = mAuth!!.currentUser
                        val uid = user!!.uid // Вот эту херь отправляй

                        val progressDialog = ProgressDialog(context)
                        progressDialog.setMessage("Вход с помощью google+...")
                        progressDialog.show()

                        changes?.clear()
                        changes?.put("firebase_user_id", uid)
                        changes?.put("lang", "RU")
                        changes?.put("country_id", selectedCountyId)
                        changes?.put("device_token", FirebaseInstanceId.getInstance().token)
                        changes?.put("device", 0)

                        disposable?.dispose()
                        disposable = apiService.value.googleSignIn(changes)
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe({ t: SignUpResponse ->

                                    if (t.status == 200){
                                        Prefs.putString("token", t.token)
                                        findNavController().navigate(R.id.action_signInFragment_to_fragmentEvents)
                                    } else {
                                        Toast.makeText(context, t.comment, Toast.LENGTH_SHORT).show()
                                    }

                                    progressDialog.dismiss()

                                }, { e ->
                                    e.printStackTrace()
                                    progressDialog.dismiss()
                                })

                    } else {
                        Log.w(TAG, "signInWithCredential:failure", task.exception)
                    }
                }
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.e("gp login", connectionResult.toString())
    }

    private fun getLocationPermission(){
        Dexter.withActivity(activity)
                .withPermissions(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .withListener(object : MultiplePermissionsListener {
                    override fun onPermissionsChecked(report: MultiplePermissionsReport?) {
                        if (report!!.areAllPermissionsGranted()){
                            fusedLocationClient.lastLocation.addOnSuccessListener {
                                if (it != null){
                                    getInfoByLocation(it.latitude, it.longitude)
                                } else {
                                    selectedCountyId = 1
                                    MainActivity.selectedCountryId = 1
                                }
                            }
                        } else {
                            selectedCountyId = 1
                            MainActivity.selectedCountryId = 1
                        }
                    }

                    override fun onPermissionRationaleShouldBeShown(permissions: MutableList<PermissionRequest>?, token: PermissionToken?) {
                        token?.continuePermissionRequest()
                    }

                })
                .check()
    }

    private fun getInfoByLocation(latitude: Double, longitude: Double){

        val progressDialog = ProgressDialog(context)
        progressDialog.setMessage("Определение страны...")
        progressDialog.show()

        changes?.clear()
        changes?.put("lang", "RU")
        changes?.put("lng", longitude)
        changes?.put("lat", latitude)

        disposable?.dispose()
        disposable = apiService.value.getCityByGeo(changes)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ t: CityResponse ->

                    if (t.status == 200){
                        selectedCountyId = t.country?.id!!
                        MainActivity.selectedCountryId = t.country?.id!!
                    } else {
                        selectedCountyId = 1
                        MainActivity.selectedCountryId = 1
                        Toast.makeText(context, t.comment, Toast.LENGTH_SHORT).show()
                    }

                    progressDialog.dismiss()

                }, { e ->
                    e.printStackTrace()
                    selectedCountyId = 1
                    MainActivity.selectedCountryId = 1
                    progressDialog.dismiss()
                })
    }

    companion object {
        private var TAG = "SignInFragment"
        private val RC_SIGN_IN = 1
    }
}
