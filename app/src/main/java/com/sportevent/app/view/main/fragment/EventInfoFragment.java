package com.sportevent.app.view.main.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.navigation.Navigation;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.sportevent.app.R;
import com.sportevent.app.adapter.PagerAdapter;
import com.sportevent.app.view.BaseFragment;

public class EventInfoFragment extends BaseFragment {

    private static final String ARG_TITLE = "arg_title";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.event_info_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.event_menu);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_ios_24px);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack(view);
            }
        });
        TextView toolbarTitle = toolbar.findViewById(R.id.toolbarTitle);
        if (getArguments() != null){
            toolbarTitle.setText(getArguments().getString(ARG_TITLE));
        }

        TabLayout tabLayout = view.findViewById(R.id.tabLayout);
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.general)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.participants)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.finance)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.feedback)));

        final ViewPager viewPager = view.findViewById(R.id.pager);

        final PagerAdapter adapter = new PagerAdapter(getChildFragmentManager());
        adapter.addFragment(new GeneralFragment());
        adapter.addFragment(new ParticipantsFragment());
        adapter.addFragment(new FinanceFragment());
        adapter.addFragment(new FeedbacksFragment());

        final LinearLayout infoLayout = view.findViewById(R.id.infoLayout);
        final CardView feedbackBtn = view.findViewById(R.id.feedbackBtn);

        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 3){
                    infoLayout.setVisibility(View.GONE);
                    feedbackBtn.setVisibility(View.VISIBLE);
                } else {
                    infoLayout.setVisibility(View.VISIBLE);
                    feedbackBtn.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        CardView wrightBtn = view.findViewById(R.id.wrightBtn);
        wrightBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("arg_title", "Sochi Hockey Open");
                Navigation.findNavController(view).navigate(R.id.chatRoomFragment, bundle);
            }
        });

        CardView participationBtn = view.findViewById(R.id.participationBtn);
        participationBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("arg_title", "Sochi Hockey Open");
                Navigation.findNavController(view).navigate(R.id.action_eventInfoFragment_to_eventAddRequestFragment, bundle);
            }
        });

        feedbackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("arg_title", "Sochi Hockey Open");
                Navigation.findNavController(view).navigate(R.id.eventAddFeedbackFragment, bundle);
            }
        });
    }

    public static EventInfoFragment newInstance(String title) {

        Bundle args = new Bundle();
        args.putString(ARG_TITLE, title);
        EventInfoFragment fragment = new EventInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }
}
